import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Favorite from 'App/Models/Favorite'

export default class FavoriteController {
  async addFavorite({ params, response }: HttpContextContract) {
    const userId = params.userId
    const movieId = params.movieId

    const favorite = await Favorite.create({
      user_id_foreign: userId,
      movie_id_foreign: movieId,
    })

  }

  async removeFavorite({ params, response }: HttpContextContract) {
    const userId = params.userId
    const movieId = params.movieId
    const favorite = await Favorite.query()
      .delete()
      .from('favorites')
      .where('user_id_foreign', userId)
      .where('movie_id_foreign', movieId)
  }

  async checkIfFavorite({ params, response }: HttpContextContract) {
    const userId = params.userId
    const movieId = params.movieId
    const favorite = await Favorite.query()
      .select('*')
      .from('favorites')
      .where('user_id_foreign', userId)
      .where('movie_id_foreign', movieId)

    if (favorite.length == 0) response.send(false)
    else response.send(true)
  }
}
