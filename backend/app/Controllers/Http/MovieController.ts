import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Movie from 'App/Models/Movie'

export default class MovieController {
  async getTopMovies({response}: HttpContextContract) {
    const movies = await Movie.query()
      .whereBetween('imdb', [8, 10])
    response.send(movies)
  }

  async getAllMovies({response}: HttpContextContract) {
    const movies = await Movie.all()
    response.send(movies)
  }

  async getSelectedMovie ({ params, response }: HttpContextContract) {
    const urlName = params.urlName
    const movie = await Movie.findBy('url_name', urlName)
    if (!movie) response.send("No movie was found by this name.")
    else response.send(movie)
  }

  async getUserFavoriteMovies({ params, response }: HttpContextContract) {
    const userId = params.userId
    const movies = await Movie.query()
      .select('movies.id', 'movies.poster_url', 'movies.movie_url', 'movies.duration', 'movies.full_name', 'movies.url_name', 'movies.description', 'movies.imdb')
      .from('movies')
      .innerJoin('favorites', 'favorites.movie_id_foreign', 'movies.id')
      .innerJoin('users','users.id', 'favorites.user_id_foreign')
      .where('users.id', userId)

    if (!movies) response.send("No favorites found for this user.")
    else response.send(movies)
  }
}
