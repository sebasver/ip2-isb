import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'
import Drive from '@ioc:Adonis/Core/Drive'
import User from 'App/Models/User'


export default class WebcamController {
  public async setImage({ params, request, response }: HttpContextContract) {
    const userId = params.userId
    const payload = request.only([
      "image"
    ]);
    console.log("uploading image")
    const buf = Buffer.from(payload.image, 'base64')
    await Drive.put('userimages/' + userId + '.jpg', buf)
    const url = await Drive.getUrl('userimages/' + userId + '.jpg')
    const user = await User.findByOrFail('id', userId)
    user.facialmatching_url = url
    user.save()
    response.send('success')
  }

  public async getPicture({ params, response }: HttpContextContract) {
    const userId = params.userId
    const imagestring = await User.findBy('id', userId)
    const imgUrl = imagestring?.facialmatching_url
    response.header("Access-Control-Allow-Origin", "*");
    if (!imgUrl) response.send("No image was found for this user")
    else response.send(imgUrl)

  }

  public async getUserId({ request, response }: HttpContextContract) {
    const payload = request.only([
      "email"
    ]);
    const userId = await User.findBy('email', payload.email)
    if (!userId) response.send("No user was found with this email")
    else response.send(userId.id)
  }
}
