import {HttpContextContract} from "@ioc:Adonis/Core/HttpContext";
import User from 'App/Models/User'

export default class AuthenticationController {

  public async register({request, response, auth}: HttpContextContract) {
    try {
      const payload = request.only([
        "username",
        "email",
        "password",
      ]);

      const user = await User.create({
        email: payload.email,
        password: payload.password,
        username: payload.username,
        avatar_url: `https://ui-avatars.com/api/?name=${payload.username}&rounded=true&background=random`
      })

      const token = await auth.use('api').login(user, {
        expiresIn: '10 days',
      })

      return token.toJSON()
    } catch (e) {
      console.log("Error: " + e)
      return response
        .send({error: {message: 'User could not be registered. (Already exists?)'}})
    }
  }

  public async login({request, response, auth}: HttpContextContract) {
    const email = await request.input('email')
    const password = await request.input('password')

    try {
      const token = await auth.use('api').attempt(email, password, {
        expiresIn: '14 days',
      });
      const user = await User.query().where('email', email).first()
      return {
        token: token.toJSON(),
        user: user
      }
    } catch {
      return response.status(400).send('User with provided credentials could not be found.')
    }
  }

  public async loginpic({request, response, auth}: HttpContextContract) {
    const email = await request.input('email')
    const user = await User
      .query()
      .where('email', email)
      .firstOrFail()

    try {
      const token = await auth.use('api').generate(user, {
        expiresIn: '14 days',
      });
      return {
        token: token.toJSON(),
        user: user
      }
    } catch {
      return response.status(400).send('User with provided credentials could not be found.')
    }
  }

  public async logout({auth, response}: HttpContextContract) {
    await auth.logout()
    return response.status(200)
  }

  async me({auth}: HttpContextContract) {
    await auth.use('api').authenticate()

    // ✅ Request authenticated
    console.log(auth.user!)
  }
}
