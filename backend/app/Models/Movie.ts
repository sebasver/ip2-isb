import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class Movie extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public poster_url: string

  @column()
  public movie_url: string

  @column()
  public duration: number

  @column()
  public full_name: string

  @column()
  public url_name: string

  @column()
  public description: string

  @column()
  public imdb: number
}

