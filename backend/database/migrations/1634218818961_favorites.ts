import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Favorites extends BaseSchema {
  protected tableName = 'favorites';

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.integer('user_id_foreign').unsigned();
      table.integer('movie_id_foreign').unsigned();
      table.primary(['user_id_foreign', 'movie_id_foreign']);
      table.foreign('user_id_foreign').references('id').inTable('users');
      table.foreign('movie_id_foreign').references('id').inTable('movies');
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
