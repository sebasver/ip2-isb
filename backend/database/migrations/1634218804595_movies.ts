import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Movies extends BaseSchema {
  protected tableName = 'movies'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table.string('poster_url', 255).notNullable().unique()
      table.string('movie_url', 255).notNullable().unique()
      table.integer('duration', 10).notNullable().unsigned()
      table.string('full_name', 255).notNullable().unique()
      table.string('url_name', 255).notNullable().unique()
      table.string('description', 255).notNullable().unique()
      table.integer('imdb', 10).notNullable().unsigned()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
