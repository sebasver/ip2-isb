/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

//Auth routes
Route.group(() => {
  Route.post('login', 'Auth/AuthenticationController.login').as('login')
  Route.post('loginpic', 'Auth/AuthenticationController.loginpic').as('loginpic')
  Route.post('logout', 'Auth/AuthenticationController.logout').as('logout')
  Route.post('register', 'Auth/AuthenticationController.register').as('register')
  Route.patch('update-avatar', 'UserController.updateAvatar')
  Route.get('google/redirect', 'Auth/AuthenticationController.redirect')
  Route.get('google/callback', 'Auth/AuthenticationController.callback')
}).prefix('api/users/')

Route.get('/', () => {
  return 'Hello world'
})

//Movie routes
Route.group(() => {
  Route.get('get-movies', 'MovieController.getAllMovies')
  Route.get('get-top-movies', 'MovieController.getTopMovies')
  Route.get('movie/:urlName', 'MovieController.getSelectedMovie')
  Route.get('get-favs/:userId', 'MovieController.getUserFavoriteMovies')
}).prefix('api/movies/')

//Favorite routes
Route.group(() => {
  Route.get('check-fav/:userId/:movieId', 'FavoriteController.checkIfFavorite')
  Route.get('rem-fav/:userId/:movieId', 'FavoriteController.removeFavorite')
  Route.get('add-fav/:userId/:movieId', 'FavoriteController.addFavorite')
}).prefix('api/favorites')

//FaceAPI routes
Route.group( () => {
  Route.post(':userId/setimage', 'WebcamController.setImage')
  Route.post(':userId/getimage', 'WebcamController.getPicture')
  Route.post('getuserid','WebcamController.getUserId')
}).prefix("api/face")
