## Adonis & Vue - Movie Streaming Application
![img.png](img.png)
#ISB3 - IP2 Project

## Setup Adonis Backend Server (:3333)
>cd backend
> 
>npm install
> 
>npm run dev

## Setup Vue-CLI Frontend Application (:3000)

>cd frontend
> 
>npm install
> 
>npm run dev

* Fixing typos/errors using lint:
> npm run lint -- --fix

