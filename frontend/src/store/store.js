import { createStore } from "vuex";
import Repository from "../repositories/RepositoryFactory";
import createPersistedState from "vuex-persistedstate";

const AuthRepository = Repository.get("auth");

const store = createStore({
  plugins: [createPersistedState()],

  state: {
    user: [],
    loggedIn: false,
  },

  getters: {
    isLoggedIn: (state) => {
      return state.loggedIn;
    },
  },

  actions: {
    async login({ commit }, payload) {
      commit("STORE_LOGGED_IN_USER", await AuthRepository.login(payload));
    },
    async loginpic({ commit }, payload) {
      commit("STORE_LOGGED_IN_USER", await AuthRepository.loginpic(payload));
    },
    async loginFromCookie({ commit }, payload) {
      commit("STORE_LOGGED_IN_USER_FROM_COOKIE", payload);
    },
    async logout({ commit }) {
      try {
        await AuthRepository.logout();
        commit("STORE_LOGGED_OUT_USER", true);
        return true;
      } catch (error) {
        console.log(error);
      }
      return false;
    },

    // eslint-disable-next-line
        async register({commit}, payload) {
      return await AuthRepository.register(payload);
    },
  },

  mutations: {
    STORE_LOGGED_IN_USER: (state, response) => {
      const { data } = response;
      if (data.status !== 400) {
        localStorage.setItem("user", data.user);
        localStorage.setItem("token", data.token);
        state.user = data.user;
        state.token = data.token;
        state.loggedIn = true;
      }
    },

    STORE_LOGGED_OUT_USER: (state, response) => {
      if (response) {
        localStorage.removeItem("token");
        localStorage.removeItem("user");
        state.user = {};
        state.token = null;
        state.insights = null;
        state.loggedIn = false;
      }
    },
  },
});

export default store;
