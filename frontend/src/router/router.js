import { createRouter, createWebHistory } from "vue-router";
import Home from "@/views/Index.vue";
import Login from "@/views/SignIn.vue";
import Register from "@/views/SignUp.vue";
import Webcam from "@/views/Webcam.vue";
import Video from "@/views/player/_video.vue";
import Error from "@/views/Error.vue";
import store from "@/store/store.js";
import Movies from "@/views/Movies";
import Favorites from "@/views/Favorites.vue";
import plslogin from "../views/plslogin";
import submit from "../views/submit";
const User = () => import("@/views/User.vue");

const routes = [
  {
    path: "/",
    name: "home",
    component: Home,
    meta: {
      title: "Vistream | Home",
    },
  },
  {
    path: "/login",
    name: "login",
    component: Login,
    meta: {
      title: "Vistream | Login",
    },
  },
  {
    path: "/register",
    name: "register",
    component: Register,
    meta: {
      title: "Vistream | Register",
    },
  },
  {
    path: "/webcam",
    name: "webcam",
    component: Webcam,
    meta: {
      title: "Vistream | Webcam",
    },
  },
  {
    path: "/plslogin",
    name: "plslogin",
    component: plslogin,
    meta: {
      title: "Vistream | login",
    },
  },
  {
    path: "/submit",
    name: "submit",
    component: submit,
    meta: {
      title: "Vistream | Submit",
    },
  },

  {
    path: "/movies",
    name: "movies",
    component: Movies,
    meta: {
      title: "Vistream | Movies",
    },
    beforeEnter(to, from, next) {
      if (store.getters.isLoggedIn) next();
      else
        next({
          name: "plslogin",
        });
    },
  },
  {
    path: "/user/:id",
    name: "user",
    component: User,
    meta: {
      requiresAuth: true,
      title: "Vistream | Profile",
    },
    beforeEnter(to, from, next) {
      if (store.state.user.id === parseInt(to.params.id)) next();
      else
        next({
          name: "login",
        });
    },
  },
  {
    path: "/favorites/:id",
    name: "favorites",
    component: Favorites,
    meta: {
      requiresAuth: true,
      metaTags: [
        {
          title: "Vistream | Favorites",
        },
      ],
    },
    beforeEnter(to, from, next) {
      if (store.state.user.id === parseInt(to.params.id)) next();
      else
        next({
          name: "login",
        });
    },
  },
  {
    path: "/player/:video",
    name: "player",
    component: Video,
    meta: {
      title: "Vistream | Streaming",
    },
    beforeEnter(to, from, next) {
      if (store.getters.isLoggedIn) next();
      else
        next({
          name: "plslogin",
        });
    },
  },
  {
    path: "/:catchAll(.*)",
    name: "error",
    component: Error,
    meta: {
      title: "Vistream | Error 404",
    },
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  to.matched.some((record) => {
    return record.meta.requiresAuth;
  });
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (!store.getters.isLoggedIn) {
      next({
        path: "/login",
        query: { redirect: to.fullPath },
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
