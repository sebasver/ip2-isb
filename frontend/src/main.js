import { createApp } from "vue";
import App from "./App";
import router from "./router/router.js";
import store from "./store/store";
import VideoBackground from "vue-responsive-video-background-player";
import axios from "axios";
import VueAxios from "vue-axios";
import "animate.css";
import WebCam from "vue-web-cam";

require("@/scss/main.scss");
import VueToast from "vue-toast-notification";
import "vue-toast-notification/dist/theme-sugar.css";
import BackgroundVideoPlayer from "vue-responsive-video-background-player";

const app = createApp(App);
app.component("video-background", VideoBackground);
app.use(router);
app.use(WebCam);
app.use(VueAxios, axios);
app.use(store);
app.config.productionTip = false;
app.use(BackgroundVideoPlayer);
app.use(VueToast, {
  position: "bottom-right",
});
app.provide("axios", app.config.globalProperties.axios);
app.mount("#app");
