import axios from "axios";

const baseDomain = `${process.env.VUE_APP_API_HOST}`;
const baseURL = `${baseDomain}/api/`;

const httpsClient = axios.create({
  baseURL,
});

httpsClient.interceptors.request.use(
  (config) => {
    let token = localStorage.getItem("token");
    if (token) {
      config.headers["Authorization"] = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default httpsClient;
