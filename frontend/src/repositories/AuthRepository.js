import Http from "./clients/AxiosClient";
const resource = "/users";

export default {
  async login(payload) {
    return await Http.post(`${resource}/login`, payload).catch(function (
      error
    ) {
      if (error.response) {
        throw error.response.data;
      }
    });
  },

  async loginpic(payload) {
    return await Http.post(`${resource}/loginpic`, payload).catch(function (
      error
    ) {
      if (error.response) {
        throw error.response.data;
      }
    });
  },

  async register(payload) {
    return await Http.post(`${resource}/register`, payload).catch(function (
      error
    ) {
      if (error.response) {
        throw error.response.data;
      }
    });
  },

  async logout() {
    return await Http.post(`${resource}/logout`).catch(function (error) {
      if (error.response) {
        throw error.response.data;
      }
    });
  },
};
