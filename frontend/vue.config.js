module.exports = {
  devServer: {
    port: 3000,
    headers: { "Access-Control-Allow-Origin": "*" },
  },
  publicPath: process.env.NODE_ENV === "production" ? "/frontend/" : "/",
};
